use piet_common::{RenderContext, Device, Color, Text, TextLayoutBuilder, TextAttribute};

fn main(){
    cfg_if::cfg_if! {
        if #[cfg(target_os = "linux")] {
            let msg = "Fonctionne sur linux";
        } else if #[cfg(target_os = "macos")] {
            let msg = "Fonctionne sur macos";
        } else if #[cfg(target_os = "windows")] {
            let msg = "Fonctionne sur windows";
        } else {
            compile_error!("could not select an appropriate backend");
        }
    }

    // Structure servant à créer la BitmapTarget. Structure différente selon la cible de compilation mais avec les même méthodes donc qui
    // s'utilise de la même façon.
    let mut device = Device::new().unwrap();
    let width = 680;
    let height = 400;

    // Créer à partir du device et qui crée le RenderContext adéquat
    let mut bitmap = device.bitmap_target(width, height, 1.0).unwrap();
    let mut rc = bitmap.render_context();
    rc.clear(Color::WHITE);

    //Cherche une police spécifique
    match rc.text().font_family("Lucida Console") {
            Some(f) => {
                    let font = f;
                    let layout = rc
                        .text()
                        .new_text_layout(msg)
                        .font(font, 20.0)
                        .default_attribute(TextAttribute::TextColor(Color::BLACK))
                        .build().unwrap();
                    rc.draw_text(&layout, (0.0, 0.0));
            },
            None => println!("Police non trouvée")
    }
    rc.finish().unwrap();
    std::mem::drop(rc);

    // La sauvegarde du fichier nécessite la features "png"
    bitmap
        .save_to_file("temp-image.png")
        .expect("file save error");
}
