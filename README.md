# Exemple d'utilisation de piet en crossplatform. 

# Prérequis 
Rust 1.47.0 minimum.
Installation de rust : https://www.rust-lang.org/tools/install

# Utilisation
``` sh

git clone https://gitlab.com/aniki02/piet-crossplatform-example.git
cd piet-crossplatform-example
cargo run
```

"temp-image.png" avec un texte dynamique selon l'OS ou est faite la compilation.
